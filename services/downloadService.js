import React from 'react';
import RNFetchBlob from 'react-native-fetch-blob';


/**
 * Function downloads the binary file and saves it to the
 * folder, which it gets as a parameter. When file is stored,
 * function returns the object that can be used as an parameter
 * to Image -component. If error happens, default image (pacman) from
 * codebase is used instead.
 *
 *
 *
 * @param {*} url
 * @param {*} filename
 */

export async function downloadFileAndReturnFilePath(url, filename) {
  return await RNFetchBlob.config({
    // response data will be saved to this path if it has access right.
    path: filename,
  })
    .fetch('GET', url, {
      //some headers ..
    })
    .then(res => {
      //console.log(JSON.stringify(res.respInfo));
      const pth = `file://${res.path()}`;
      console.log('Image source ' + pth);
      return {uri: pth};
    })
    .catch(error => {
      //In the case of error, show pacman instead.
      //use image resource inside app folder
      //console.log(JSON.stringify(error));
      const icon = require('./pacman.png');
      return icon;
    });
}

/**
 * Same is here done with callback-function. Callback-function is
 * provided as a parameter by the calling function. If can be e.g.
 * state function (Hook).
 *
 * @param {} url
 * @param {*} filename
 * @param {*} callback
 */

export function downloadFileWithCallback(url, filename, callback) {
  RNFetchBlob.config({
    // response data will be saved to this path if it has access right.
    path: filename,
  })
    .fetch('GET', url, {
      //some headers ..
    })
    .then(res => {
      //console.log(JSON.stringify(res.respInfo));
      const pth = `file://${res.path()}`;
      console.log('Image source ' + pth);
      callback({uri: pth});
    })
    .catch(error => {
      //In the case of error, show pacman instead.
      //use image resource inside app folder
      //console.log(JSON.stringify(error));
      const icon = require('./pacman.png');
      callback(icon);
    });
}
