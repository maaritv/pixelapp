# Pixelmonster app #

App is a React native sample app containing examples of:

* Fetching binary data (images) using Fetch Blob -library
    * [react-native-fetch-blob in npmjs.com](https://www.npmjs.com/package/react-native-fetch-blob)
* Using cache-folder to store images. Cache folder is part of internal storage, which is not accessible for
other apps. It is also encrypted in new Android versions. [See documentation:](https://developer.android.com/training/data-storage/app-specific) 
* Showing image in Image-component
* Using Hooks; useState and useEffect in the function component

