/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { useState, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
} from 'react-native';

import { downloadFileAndReturnFilePath } from './services/downloadService';
import { MonsterComponent } from './components/MonsterComponent';
var RNFS = require('react-native-fs');

export const App = () => {
  const [pixelImage, setPixelImage] = useState(null);
  const [imageNbr, setImageNbr] = useState(0);

  useEffect(() => {
    let mounted = true;
    refreshPixels();
    return () => mounted = false
  }, []);

  const refreshPixels = async () => {
    setImageNbr(imageNbr + 1);
    //Template string variable
    const fileName = `${RNFS.CachesDirectoryPath}/pixels${imageNbr}.png`;
    await downloadFileAndReturnFilePath('https://app.pixelencounter.com/api/basic/monsters/random/png?size=200', fileName)
      .then(fle => {
        setPixelImage(fle);
      });
    //or
    //downloadFileWithCallback('https://app.pixelencounter.com/api/basic/monsters/random/png?size=200', fileName, setPixelImage)
  };

  return (
    <View>
      <Text style={styles.headerStyle}>Monster of the day</Text>
      <MonsterComponent pixelImage={pixelImage} refreshPixels={refreshPixels} />
    </View>
  )
};

const styles = StyleSheet.create({

  headerStyle: {
    fontSize: 26,
    color: '#E61834',
    marginBottom: '10%',
  },
  pacmanStyle: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
