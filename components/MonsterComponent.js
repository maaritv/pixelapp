import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

export const MonsterComponent = ({pixelImage, refreshPixels}) => {
  return (
    <View>
      <Image style={styles.imageStyle} source={pixelImage} />
      <TouchableOpacity style={styles.button} onPress={refreshPixels}>
        <Text style={styles.buttonText}>Fetch next pixel character</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    fontFamily: 'Arial',
    textAlign: 'center',
  },
  button: {
    height: 90,
    width: '50%',
    alignSelf: 'center',
    backgroundColor: '#A891BE',
    fontSize: 24,
    fontWeight: '600',
    alignItems: 'center',
    paddingTop: 30,
  },
  imageStyle: {
    width: 400,
    height: 400,
    marginBottom: 20,
  },
});
